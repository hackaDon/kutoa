# Le HackaDon 2018

Bienvenue sur le site du HackaDon 2018 de Kutoa :D

# Rappel de la problématique
Comment faciliter l’engagement solidaire des plus jeunes auprès des associations ?

# Notre mojo
La vie associative au quotidien

# Principe global
Application permettant de découvrir les missions associatives autour de soi. En réalisant des missions proposées par les associations partenaires et défis donnés par Kutoa, l’utilisateur gagne des points lui permettant de débloquer des cadeaux, des accès à des évènements exclusifs, des certifications qui pourront être utilisées dans le cadre d’un CV par exemple. 

# Nos cibles
Les jeunes de - 25 ans qui ont une sensibilité associative mais qui ne savent pas comment s’y prendre. 
Les associations avec une petite notoriété qui recherchent des bénévoles qualifiés et plus de visibilité.
Les associations avec une grosse notoriété qui cherchent des bénévoles qualifiés. 

# Description de l’application

L’utilisateur, géolocalisé, découvre les missions autour de lui. Il peut les filtrer par préférences, types, lieux, difficulté, temps … 
La réalisation d’une mission permet de gagner un certain nombre de points, débloquant des badges. 
Plus on a de points et de badges plus on peut accéder à des cadeaux, places de cinémas, expositions, réductions, accès à des évènements associatifs, ...

L’utilisateur peut également gagner des points en réalisant les défis quotidiens proposés par Kutoa. Ces défis gamifie l’acte solidaire, le rend plus accessible, simple et adapté à ses possibilités. 
L’utilisateur peut également accéder à son historique de missions grâce à son carnet de bord associatif. Ce carnet de bord permet également de se rendre compte de l’impact de ses petites (ou grandes actions). 
Ex : "En tenant un stand au solidays tu as permis d'éviter 85 rapports non protégés". 
Plus l’utilisateur réalise de missions et de défis, plus nous établissons ses préférences et grâce au machine learning nous pouvons proposer des missions de plus en plus adaptées à son profil. 

Côté associations : 
Via notre application nous serons capable d’identifier des profils pouvant devenir des bénévoles particulièrement engagé à l’avenir. 
En croisant les intérêts de ces personnes et les besoins des associations nous pourrons faire matcher ces bénévoles en devenir avec l’associations faites pour eux. 
Kutoa pourra ainsi pousser de manière encore plus cibler cette association à l’utilisateur concerné afin qu’il s’engage à long terme. 
L’association gagne donc des bénévoles qualifiés, réellement prêt à s’investir. 

# Validation des missions 

Les bénévoles se munissent d'un QR code, scanné par un responsable de l'association après la mission pour la valider. 
Système de contrat de la blockchain. 

# Répartition des points

Les missions rapportent plus de points que les défis. 
Pour calculer les points nous faisons un ratio temps passé / compétences requise. 

# Notre spot de publicité 

https://www.youtube.com/watch?v=BZxhRXKOR-c&feature=youtu.be

# Notre présentation

https://app.ludus.one/4678f14a-89d8-48d8-893b-33af66475779#1
