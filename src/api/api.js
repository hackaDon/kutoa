class api {
    async getJobSubject(s, ss = 5, iap = 'LYTb6H7ylJFq1FuDFlhPjrMDILbqe0bK') {
      const reponse = await fetch('https://marietailpied-eval-test.apigee.net/jobs-and-skills/suggest?' +
          'query='+s+'' +
          '&size='+ss+'' +
          '&apikey='+iap);
      const json = await reponse.json();
      return json;
    }
}
export default new api();