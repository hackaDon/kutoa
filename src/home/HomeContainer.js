import React, { Component, Fragment, createRef } from 'react';
import api from "../api/api.js";


class HomeContainer extends Component {
    state = {
        data: null,
        response: false
    }

    inputApiKey = createRef();
    search = createRef();
    sizeSearch = createRef();



    async updateSearch(s, ss, ak) {
        var reponse = await api.getJobSubject(s, ss, ak);
        var json = reponse.data.concepts.edges;
        this.setState({
            reponse: true,
            data: json
        });
        this.generateResponse(json);
        console.log(json);
        return json;
    }
    generateResponse = (data) => {
        return(
            <div className={'response-container'}>
                {data.map(response => {
                    return(
                        <div className={'card'}>
                            <div className={'icon'}></div>
                            <div className={'descr'}>
                                <span>{response.node.prefLabel}</span>
                                <a href={'/Fiche/'+response.node.prefLabel.replace(' ' ,'-').replace('/', '-')}>Lien vers fiche</a>
                            </div>
                        </div>
                    )
                })}
            </div>
        );
    }
    handleSubmit = (e) => {
        e.preventDefault();
        var inAK = this.inputApiKey.current.value;
        var inSearch = this.search.current.value;
        var inSizeSearch = this.sizeSearch.current.value;

        var rep = this.updateSearch(inSearch, inSizeSearch, inAK);
    };
    render() {

        return(
            <div>
                <div className={'container'}>
                    <div className={'background'}></div>
                    {!this.state.reponse &&
                        <form onSubmit={this.handleSubmit}>
                            <div className={'form-group'}>
                                <label>Word search</label>
                                <input ref={this.search} type={'text'} name={'search'} placeholder={'Search'}/>
                            </div>
                            <div className={'form-group'}>
                                <label>Size</label>
                                <input ref={this.sizeSearch} type={'text'} name={'size'} placeholder={'Size'}/>
                            </div>
                            <div className={'form-group'}>
                                <label>Api Key</label>
                                <input ref={this.inputApiKey} type={'text'} name={'apiKey'} placeholder={'API KEY'} value={'LYTb6H7ylJFq1FuDFlhPjrMDILbqe0bK'}/>
                            </div>
                            <div className={'form-group'}>
                                <input type={'submit'}/>
                            </div>
                        </form>
                    }
                    {this.state.reponse &&
                        <div>{this.generateResponse(this.state.data)}</div>
                    }
                </div>
            </div>
        )
    }
}

export default HomeContainer;