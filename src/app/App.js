import React, {Component, Fragment} from 'react';
import '../App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import HomeContainer from "../home/HomeContainer";
import Fiche from "../home/Fiche";

class App extends Component {
    render() {
        return (
            <div className="App">
                <Router>
                    <Fragment>
                        <Route exact path="/" component={HomeContainer}/>
                        <Route path={"/Fiche"} component={Fiche}/>
                    </Fragment>
                </Router>
            </div>
        );
    }
}

export default App;
